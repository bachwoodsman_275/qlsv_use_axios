// listSV = [];
// var ShowData = localStorage.getItem("DATA");
// if (ShowData !== null) {
//   listSV = JSON.parse(ShowData).map(function (item) {
//     return new Sinhvien(
//       item.ma,
//       item.ten,
//       item.email,
//       item.pass,
//       item.toan,
//       item.ly,
//       item.hoa
//     );
//   });

//   redenListSV(listSV);
// }
var isSinhVien = null;
tatLoading();
function fetchSvList() {
  batLoading();
  svServ
    .getList()
    .then(function (res) {
      redenListSV(res.data);
      tatLoading();
    })
    .catch(function (err) {});
}
fetchSvList();
// save local
// function saveLocalrange() {
//   redenListSV(listSV);
//   var data = JSON.stringify(listSV);
//   localStorage.setItem("DATA", data);
// }
function themSinhVien() {
  batLoading();
  var newSv = layThongtinTuForm();
  svServ
    .create(newSv)
    .then(function (res) {
      fetchSvList();
    })
    .catch(function (err) {
      tatLoading();
    });
  saveLocalrange();
  document.querySelector("#formQLSV").reset();
}

function XoaSinhVien(ma) {
  batLoading();
  svServ
    .delete(ma)
    .then(function (res) {
      fetchSvList();
    })
    .catch(function (err) {});
}
function SuaSinhvien(id) {
  batLoading();
  isSinhVien = id;
  document.getElementById("txtMaSV").disabled = true;
  svServ
    .getById(id)
    .then(function (res) {
      ShowThongtinlenform(res.data);
      tatLoading();
    })
    .catch(function (err) {});
}
function capNhatSinhVien() {
  batLoading();
  var sv = layThongtinTuForm();
  svServ
    .update(isSinhVien, sv)
    .then(function (res) {
      fetchSvList();
    })
    .catch(function (err) {});
  document.getElementById("txtMaSV").disabled = false;
  document.querySelector("#formQLSV").reset();
}
// var search = document.getElementById("txtSearch");
// search.onkeyup = function timKiemSinhVienTheoTen() {
//   var renderSvTheoName = [];
//   var input = search.value;
//   if (input.length > 0) {
//     listSV.forEach(function (sv) {
//       var ten = sv.ten;
//       if (ten.includes(input)) {
//         renderSvTheoName.push(sv);
//         redenListSV(renderSvTheoName);
//       }
//     });
//   } else {
//     redenListSV(listSV);
//   }
// };
function Reset() {
  listSV = [];
  redenListSV(listSV);
  saveLocalrange();
}
