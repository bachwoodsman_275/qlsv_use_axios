const BASE_URL = "https://6484a22bee799e321626e21a.mockapi.io/sv";
var svServ = {
  getList: function () {
    return axios({
      url: BASE_URL,
      method: "GET",
    });
  },
  create: function (sv) {
    return axios({
      url: BASE_URL,
      method: "POST",
      data: sv,
    });
  },
  delete: function (ma) {
    return axios({
      url: `${BASE_URL}/${ma}`,
      method: "DELETE",
    });
  },
  getById: function (ma) {
    return axios({
      url: `${BASE_URL}/${ma}`,
      method: "GET",
    });
  },
  update: function (ma, sv) {
    return axios({
      url: `${BASE_URL}/${ma}`,
      method: "PUT",
      data: sv,
    });
  },
};
